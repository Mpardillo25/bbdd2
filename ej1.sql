DROP DATABASE Tienda_Movil;

CREATE DATABASE IF NOT EXISTS Tienda_Movil;

USE Tienda_Movil;

CREATE TABLE CLIENTE (
    DNI VARCHAR(9),
    Nombre VARCHAR(10),
    Apellidos VARCHAR(20),
    Telefono INT,
    EMAIL VARCHAR(30),

    PRIMARY KEY (DNI)
);
