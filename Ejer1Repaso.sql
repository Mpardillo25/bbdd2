DROP DATABASE IF EXISTS Ejer1Repaso;

CREATE DATABASE Ejer1Repaso;
USE Ejer1Repaso;

CREATE TABLE Ciudad (
    Codigo INT UNSIGNED AUTO_INCREMENT,
    Nombre VARCHAR(30) UNIQUE,
    
    CONSTRAINT Ciudad_PK PRIMARY KEY (Codigo)
);

CREATE TABLE Temperatura (
	Dia DATE,
	Codigo_Ciudad INT UNSIGNED,
	Grados DECIMAL(3,1),

	CONSTRAINT Temperatura_PK PRIMARY KEY (Dia,Codigo_Ciudad),
	CONSTRAINT Temperatura_Ciudad_FK FOREIGN KEY (Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);

CREATE TABLE Humedad (
	Dia DATE,
	Codigo_Ciudad INT UNSIGNED,
	Porcentaje TINYINT UNSIGNED,

	CONSTRAINT Humedad_PK PRIMARY KEY (Dia,Codigo_Ciudad),
	CONSTRAINT Humedad_Ciudad_FK FOREIGN KEY (Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);

CREATE TABLE Informe (
	ID INT UNSIGNED AUTO_INCREMENT,
	Dia DATE,
	Codigo_Ciudad INT UNSIGNED,

	CONSTRAINT Informe_PK PRIMARY KEY (ID),
	CONSTRAINT Informe_Humedad_FK FOREIGN KEY (Codigo_Ciudad) REFERENCES Ciudad(Codigo)
);


