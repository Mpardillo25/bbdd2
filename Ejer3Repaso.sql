DROP DATABASE IF EXISTS Ejer3Repaso;
CREATE DATABASE Ejer3Repaso;
USE Ejer3Repaso;

CREATE TABLE SOCIOS (
    Nombre VARCHAR(20) NOT NULL,
    Direccion VARCHAR(30),
    N_Telefono CHAR(15),
    Fecha_inscripcion DATE,
    N_Socio INT AUTO_INCREMENT,

    CONSTRAINT SOCIOS_PK PRIMARY KEY (N_Socio)
);

CREATE TABLE LIBROS (
    Titulo VARCHAR(30) NOT NULL,
    Autor VARCHAR(20),
    Fecha_editado DATE,
    N_Libro BIGINT AUTO_INCREMENT,

    CONSTRAINT Libro_PK PRIMARY KEY (N_Libro)
);
CREATE TABLE PRESTAMOS (
    Fecha_retiro DATE,
    Fecha_entrega DATE,
    N_Socio INT UNSIGNED,
    N_Libro BIGINT,
    IDPrestamo BIGINT AUTO_INCREMENT,

    CONSTRAINT PRESTAMOS_PK PRIMARY KEY (IDPrestamo)
);

ALTER TABLE PRESTAMOS ADD CONSTRAINT PRESTAMOS_SOCIOS_FK FOREIGN KEY (N_Socio) REFERENCES SOCIOS(N_Socio);
ALTER TABLE PRESTAMOS ADD CONSTRAINT PRESTAMOS_LIBROS_FK FOREIGN KEY (N_Libro) REFERENCES LIBROS(N_Libro);

INSERT INTO SOCIOS(Nombre, Direccion, N_Telefono, Fecha_inscripcion, N_Socio) VALUES ('Pedro Gil', 'Canelones','123.456.789 ', '1984,12-12');













































































